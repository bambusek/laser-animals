# Laser engraved & cut animals

This repository contains source files for laser engraved and cut animals. Laser settings that can be found below are tested on Trotec Speedy 300 while using aspen 4mm thick plywood. Source files were created in CorelDraw 2018 (`.cdr` files) and are also exported to `.pdf`.

## Laser settings

Color |  | Mode | Power | Speed
----- | --- | ---- | ----- | ----- 
Black |  ![#00FFFF](https://placehold.it/25/000000/000000) | Engrave | 50 | 30
Red | ![#FF0000](https://placehold.it/25/FF0000/FF0000) | Engrave | 50 | 40
Blue |  ![#0000FF](https://placehold.it/25/0000FF/0000FF) | Engrave | 50 | 60
Cyan |  ![#00FFFF](https://placehold.it/25/00FFFF/00FFFF) | Engrave | 50 | 85
Yellow |  ![#FFFF00](https://placehold.it/25/FFFF00/FFFF00) |Cut | 30 |75
Magenta | ![#FF00FF](https://placehold.it/25/FF00FF/FF00FF) | Cut | 100 | 1

